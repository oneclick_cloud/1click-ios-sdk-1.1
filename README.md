# README #

Thank you for downloading 1click iOS SDK for WebRTC.

This repository contains,

* static library lib1clickIosSdk.a
* header file _clickIosSdk.h.

An iOS application must include static library lib1clickIosSdk.a and header file _clickIosSdk.h.

An example application is available at https://bitbucket.org/oneclick_cloud/1click-rtc-app 

Please read:

* [Developer Environment Notes](https://bitbucket.org/oneclick_cloud/1click-ios-sdk-1.1/overview#markdown-header-developer-environment-notes)
* [FAQ](https://bitbucket.org/oneclick_cloud/1click-ios-sdk-1.1/overview#markdown-header-faq)
* [API USAGE](https://bitbucket.org/oneclick_cloud/1click-ios-sdk-1.1/overview#markdown-header-api-usage)


Developer Environment Notes:
----------------------------
----------------------------    
              
1. __1Click supports only amrv7 architecture currently.__The underlying codec/third-party libraries are 32bit supported and it would take some time to move it to 64bit eco-system.We are still compiling 32 bit binary. A 32bit application will be able to leverage all the processing, that is available on the new iPad. Please make note of the following points regarding the architecture in Build Settings.

        a) Click on the Project in Xcode.
        b) Go to the Build Settings tab in the editor.
        c) Then find the "Valid Architectures" and "Build Active Architecture Only" settings (under the Architectures category).
        d) "Valid Architectures" should only contain "armv7" (remove "arm64" and "armv7s" if they are present).
        e) "Build Active Architecture Only" should be set to NO.         

2. Go to the Build Settings tab in the editor. Add __-ObjC__ to ___Other Linker Flags___ option.

3. Set the __deployment target to 7.0(or above)__ to avoid any compiler warnings related to armv7 architecture.       
                                   
4. __Xcode simulator does not support camera__, hence we cannot test the 1Click iOS SDK on the simulator.
You will need to run the app on the device to test the video calling features. However you can test the rest of your app on the simulator selectively(using conditional compiling).      
  When you are working with a static library for an iPhone project in Xcode, you can either have  one fat binary that contains code for both the iPhone device and the iPhone simulator in the same file, or you may have two different files which you will have to add to Xcode.Thus, you can separate the two  libraries and there by do a conditional compilation.     
   __Know more about conditional compiling from [here](http://stackoverflow.com/questions/1211854/xcode-conditional-build-settings-based-on-architecture-device-arm-vs-simulat)__   
                           
5. __Add the following libraries( required during the compilation)__:  
   Goto Build Phases -> Link Binary with Libraries and add the following:  
    * AVFoundation.FrameWork
    * AudioToolBox.FrameWork
    * CoreMedia.FrameWork
    * GLKit.Framework
    * CoreGraphics.Framework
    * UIKit.Framework
    * Foundation.Framework
    * libicucore.dylib
    * libstdc++.6.dylib
    * libsqlite3.dylib
    


FAQ:
--------
---------

### 1. What do we intend to provide with our iOS SDK 
1Click is the best video conferencing solution for your business. We help you customize your web 
conference rooms, record video calls, conduct multipoint face-to-face chat seamlessly using our 
service. 1Click iOS SDK aims at providing the complete video infrastructure for the apps developed 
for the different Ipad/iPhone versions.
                 
Our focus has been on providing the developer with all features so that he comes out with amazing apps that cater to all the video calling needs of the users. 
So go ahead and experience the power of real time communication- powered by 1Click!

### 2. Does the SDK support all Apple models effectively? 
Though the SDK performs well with earlier models like iPhone 4 or iPad 2 by providing audio 
support and low resolution video; But the best performance can be achieved on the new models. 
                         
There can be a scenario where the customer is using an older device say an Ipad2 with 
an A5 chip. This is much slower than an A6 or A7 chip. But our API is designed to support 
lower version as well and will surely support a device with an A5 chip. You can expect good 
call quality and minimum frame drop(if any). 

### 3. How to get access to the API?    
 
To get started,       
Currently 1click.io and us.1click.io are different servers. You can test ios SDK only on us.1click.io.

i. you need to sign up with us at [us.1click.io](https://us.1click.io/) . Once you are logged in you will be provided with the API access key.The API key helps 1Click to uniquely identify you(your app) and allow access to the resources on the server.     
                    
ii. Next, you can check out the API documentation. There are README(s) attached to each Git explaining you the API.          
           
iii. You can access our SDK [here](https://bitbucket.org/oneclick_cloud/1click-ios-sdk-1.1) and our sample app [here](https://bitbucket.org/oneclick_cloud/1click-rtc-app).
                   
iv. Compile the code in Xcode after reading the Developer Environment Notes.      
  

### 4. What are the different options to create a room?             
      
a. You can use ___createRoom___ API, which creates room with 'peer' configuration 512kbps 
and size 2. This caters to the peer to peer calling alone and hence only 2 people can be involved in 
the call.The room name should be alphanumeric and must not include any special characters.         
           
___(void) createRoom:(NSString *) roomName;___    
           
b. If there is a need for multiway/broadcast calling we have a separate api for this - 
___createRoomWithSettings___. This too supports a bandwidth of 512kbps and can accommodate up to 
13 participants simultaneously. There is no option to specify resolution . Resolution will be picked up by media server based upon bandwidth.    

For a multiway call, if   
           Bandwidth < 256kbps ; Resolution: 320x240,    
           Bandwidth < 512kbps ; Resolution: 640x480,    
           other wise  Resolution:1280x720 .      
  
Though we have the above createRoom API for peer to peer call, the createRoomWithSettings API also lets us set up a peer-to-peer call ( apart from multiway and broadcast) and gives us better 
options to control the various parameters involved.      
             
___ (void) createRoomWithSettings:(NSString*)roomName bitrate:(NSNumber*)bitrate 
frameRate:(NSString*)frameRate type:(NSString*)type size:(NSNumber*)size;___ 

### 5. When creating a room on web, what does Layout mean and what are the values associated it?  
A Layout generally refers to the way a screen is divided. In case of video call it refers to how the 
video on the screen is divided among the caller and the participants. The concept of layout does not 
make sense to a Peer-to-peer call. However the parameter applies to a multiway call.

There are two values which can be set for the Layout- Static and Dynamic.

        *"Static" divides remote video into equally where all participants occupy equal size.
        *"Dynamic" adapts based on number of participants. 

### 6. How do you go about with the video chat after the room is created?
Once the room is created, you need to join the room with the following API. You can specify if the 
intended call need audio/video or both. There are options to select the camera type (front/back) and 
even the resolution. The resolution applies to the local capturing video only. It takes the following 
values- 352, 640, 1280, 1920 . Any other values will result in a resolution of 640x480.   
       

___(void) joinRoom:(NSString *)roomName pin:(NSString *) pin callType:(NSString *)callType 
videoParams:(NSDictionary *)videoParams remoteView:(UIView*)remoteView 
localView:(UIView*)localView;___

Once you have joined the room, you need to invite the participant(s) to the call. You can invite 
multiple participants by providing the email ids of the participants as a comma separated value in 
the following API.     
        
___(void) invite:(NSString *)roomName emailList:(NSString *)emailList;___

### 7. What is the maximum number of concurrent peer-to-peer chats that is supported? 
The number of concurrent chats supported depend upon the plan chosen. Please get in touch with us <info@1click.io> to know more about the rate plan.

### 8. How is dropping/reconnecting of the call handled?
The exitRoom API will end the call. We have option to reconnect as well. If the call gets 
disconnected at the remote end, then the call is reconnected as soon as the participant at the remote 
end logs in again. 
      
___(void) exitRoom;___
                
Example:

    a.  Alice joins Room "shopping".
    b.  Bob joins Room "shopping".    
    c.  both are in call.
    d.  Bob exit Room "shopping".(Dropping the call.)    
    e.  Bob joins Room "shopping".     
    f.  Automatic call reconnect.        


### 9. For a phone client:         
###       i. When a connection is established, will there be a bandwidth consumption automatically?
When you are waiting in the room, bandwidth will not be consumed. Once your call connected to the server, it will consume bandwidth.
###      ii. While delivering the streams, do we use a separate socket for each stream or a single socket is used, with all the streams packaged together?  
In a multiway call, streams are mixed and packaged to single socket delivery.
###     iii. Does the server continue to broadcast the video/audio even when the client has switched off the audio/video?
Audio mute is supported. But video mute is not supported currently. So the server will send black screen as video output, though the bandwidth used will be less than 50kbps.
###    iv. How do you implement 'Push to chat'?
'Push to chat' will be implemented using offline video messages. We do not support this currently, but plan to support this with the next release.

### 10. What is the maximum bandwidth that is supported for the video? 
You can configure a conference with different bandwidths. A bandwidth of 512kbps will be easily 
supported which can go even up to 1Mbps. You can have the option to configure to higher bit rate in 
the UI. The createRoom API lets you specify the video bandwidth. If you have specific requirement, get in touch with us <info@1click.io>.

### 11. How do you calculate the total bandwidth and the video bandwidth?
You need to determine the available bandwidth first and then arrive at the video bandwidth value. 
Bandwidth specified in the API only refers to video. For example if you need a bandwidth of 800kbps for  your video, you should have a total bandwidth greater than 800kbps because:                    


                   
           Total bandwidth  = video bandwidth + audio bandwidth + rtcp/stun control                                  
                            = 800 + 32 + 32 ~kbps                                         
                            = 864 kbps                                   

### 12. Is it possible to do any pre-processing to the captured video? 
We can surely pre-process the video, with activities like motion detection and blurring. Motion 
detection on video frame is a CPU intensive activity and activities like blurring the image will add 
to the load on to the CPU. This will lead to a bad video calling experience. 

### 13. What is the default resolution for the sending video stream? What are the predefined resolutions you can expect for a P2P video capturing? 
The default resolution offered is 4:3. There are multiple resolution supported with different aspect ratio, based on resolution of the input video.
The resolution can be specified as  per the need and we can support this resolution at the time of 
encoding. 
Following is the resolution offered on iPad/iPhone:   
> > NSString \*const AVCaptureSessionPreset320x240;   
> > NSString \*const AVCaptureSessionPreset352x288;     
> > NSString \*const AVCaptureSessionPreset640x480;   
> > NSString \*const AVCaptureSessionPreset960x720;   
> > NSString \*const AVCaptureSessionPreset1280x720;      

               
We are in favour of the following resolutions- 352*288, 640 * 480, 1280 * 720. We would suggest 
to use any of above 3 resolutions. Even though a resolution of 1920 * 1080 is also supported, most 
of the devices are not powerful enough to handle this resolution. We believe the iPad form factor is 
ideal with 480p (640x480) or 720p(1280x720) resolution.

### 14. The source video, meeting room and remote display area do not always have the same 
aspect ratio. When will crop and re-size of the video frame happen? 
Crop and resize happens on client when rendering. We read dimension of incoming of video frame 
and determine aspect ratio of both remote video and local video. While rendering it on UIView, AVMakeRectWithAspectRatioInsideRect function maintains video stream dimensions and scaling to the UIView specified.      
               
AVMakeRectWithAspectRatioInsideRect(videoAspectRatio, UIView.bounds);   
> videoAspectRatio = captured aspect ratio of video camera.    
> UIView.bounds = width and height of UIView where it is going to be rendered.      
              
iOS doesn't maintain aspect ratio in portrait mode and so the width and height gets swapped. Front camera will always be landscape left for iOS, therefore, 1280x720 resolution in landscape mode comes as 720x1280 in portrait mode. Hence, sometimes you see video stretched.

### 15. Is there an option to change the aspect ratio of the video sent on the wire? Can we match the aspect ratio before streaming rather than cropping afterwards?
Yes, it is possible. Please get in touch with us <info@1click.io>, if you have a specific requirement.

### 16. Is it possible to manually alter the resolution of the video stream of the publisher?
Yes, it is possible. Please get in touch with us <info@1click.io>, if you have a specific requirement.

### 17. How do you achieve proper orientation for the video stream for each device with a different aspect ratio?
When the device is rotated, the video frame will be rotated to maintain 'world is always up' during the call. Please use the following API for orientation of the video frame on the device.  
    
___(void) rotateVideo;___   



### 18. If there is lag in the video, is the transmitted audio also delayed? Can they be out of sync? 
If there is delay in video, audio will also be delayed so they are in sync. The delay always depends on the round trip time from device to 1click media server. Based on this jitter buffer is allocated, to counter packet loss. When there is high packet loss, audio video might go out of sync. But periodically audio, video will be synced using rtcp packets, which is generally 5-7 seconds.

### 19. How do you get the packet loss notification?
Please use the following API to get the notification for the audio and video loss respectively.The API reports the loss when there is audio/video packet loss greater than 5%.   
- ___(void) audioLoss:(NSNumber*)loss;___   
- ___(void) videoLoss:(NSNumber*)loss;___    


### 20. Can we switch the front and back camera?
You can use the switchCamera API to switch between the front and back cameras during the call.      
___(void) switchCamera;___

### 21. Is it possible to mute the microphone programatically, during a session?
Yes, You can use the muteAudio API to mute the microphone.You can set the parameter 'enable' as true to mute the microphone and false to un-mute it. The API returns true if the operation was successful and false otherwise.      
   
___(BOOL) muteAudio:(BOOL)enable;___ 



### 22. When I use the iOS SDK for video chat, I find that the voice is repeated with a delay of 1-2 seconds. What is the  reason for this echo?
If both devices are placed close to each other, you can expect an echo because of feedback. So if the  sender and  the receiver are in the same room for example, voice will be fed back into mic, which creates echo. Always make sure the  devices are at a considerable distance(may be in separate rooms) to avoid feedback echo.

### 23. Is there an option to connect and use external webcam( USB web cam or ip-cam) for the video chat?
The iOS app will not support an external web cam. But you can use a web cam when using a browser to chat.

### 24. Does the SDK access the UDID  or the MAC address of the device?
No we do not access the UDID or the MAC address  of the device.

### 25. How will the SDK support a scenario where two users want to connect to a single session. But one of the users is using an iphone and the other user is on his pc?
Yes this is possible. Both the users need to join the same room.The pc user will have to login to the browser(say chrome). if , for example the room name is MyRoom, he will need to use https://us.1click.io/webdash/index.html#/Room/MyRoom to login.
The ipad/iphone user on the other hand should login using the SDK-joinRoom- may be click joinRoom:@"MyRoom".

### 26. How is the user notified when another user is trying to call him?
You can use your App push notification system to let the other user join the room.
1click platform supports inviting other user via email. The API supports entering the email ids for multiple users as a comma separated value. 

### 27. Which browser is better in handling the video frame- Chrome or Firefox? 
Currently we recommend Chrome for a better video experience. 
               
You might end up with minor glitches if you are using a combinations of ipad2 + Firefox(OSX). Firefox doesn't let your application limit the bandwidth. Firefox usually goes aggressive 
with video bandwidth and starts streaming at anything like 500kbps up to 2Mbps, which results in 
video packet loss. Also with a high video bandwidth+ audio+ other overheads, you end up 
streaming to lesser number of users even with a decent connection. 
The WebRTC fans are waiting for Firefox to fix it (but it has been a long wait.).
                          
On the other hand, chrome lets you limit (specify) the receive bandwidth. In this way, with a 
predictable bandwidth you can easily do capacity planning for servers based on the number of 
receivers.

# API USAGE #

clickNotificationDelegate gives callback for application regarding call events/messages.

clickNotificationDelegate is a protocol that must be implemented by Application Object.

methods to implement

1)
```objective-c

    -(void) sendNotification:(NSNumber*)status msg:(NSString*) msg; 
```

        parameters -

              status -- 0 failure, 1 success.

              msg -- message for your activity back from server.

2)
```objective-c
    -(void) callWait;
```
        Waiting for other participant to join the room.

3)
```objective-c
    -(void) callJoin;
```
        Other participant has entered the room.

4)
```objective-c
    -(void) callConnected;
```
        Call is connected.

5)
```objective-c
    -(void) callDisconnected;
```
        Call is Ended.

6)
```objective-c
    -(void) remoteHangUp;
```
        Other participant has left the room.

7)
```objective-c
-(void) audioLoss:(NSNumber*)loss;
```
        Percentage of audio packets lost reported when its greater than 5%.

8)
```objective-c
-(void) videoLoss:(NSNumber*)loss;
```
        Percentage of video packets lost reported when its greater than 5%.

9)
```objective-c
-(void) didLocalVideoStart;
```
        Camera has been captured and local video started.

10)
```objective-c
-(void) didRemoteVideoStart;
```
        Remote party video is started.

# Object Instantiation

_clickIosSdk object offers following api's:


------------------

0)
```objective-c
-(_clickIosSdk *) initWithDelegate:(id<clickNotificationDelegate>)delegate email:(NSString *)email apiKey:(NSString *)apiKey;
```
        parameters -

            delegate - Application object that implements clickNotificationDelegate callbacks.

            email - login email for us.1click.io

            apiKey - apikey for above email

1)
```objective-c
-(void) joinRoom:(NSString *)roomName pin:(NSString *) pin callType:(NSString *)callType videoParams:(NSDictionary *)videoParams remoteView:(UIView*)remoteView localView:(UIView*)localView;
```
        parameters - 
            roomName - Name of room.

            pin - future usage, please send @"" now.

            callType - allowed values are @"video", @"audio", @"audience".

                       @"video" -- participant will send and recv video.

                       @"audio" -- participant will send and recv audio.

                       @"audience" --- participant will send only audio, receive video and audio from other participant.

            videoParams - NSDictionary with allowed keys [@"resolution", @"camera"].

                        @"resolution" key will take following accepted values are [@"352", @"640", @"1280", @"1920"].

                        @"352" resolution refers to -- 352 x 288
                        @"640" resolution refers to -- 640 x 480
                        @"960" resolution refers to -- 960 x 720
                        @"1280" resolution refers to -- 1280 x 720
                        @"1920" resolution refers to -- 1920 x 1080

                        @"camera" key will take following accepted values are [@"front", @"back"].

                        @"front" --  will use front camera for video capturing.
                        @"back" -- will use back camera for video capturing.

            remoteView - UIView which will be used to display remote video.
            localView - UIView which will be used to display local video.      

2)
```objective-c
-(void) exitRoom;
```
        exitRoom will end the call.

3)
```objective-c
-(void) createRoom:(NSString *) roomName;
```
        parameters -
            roomName - should be alphanumeric characters without special characters.
        will create Room with peer-peer call type with maximum size of 2 people.

4)
```objective-c
-(void) createRoomWithSettings:(NSString*)roomName bitrate:(NSNumber*)bitrate frameRate:(NSString*)frameRate type:(NSString*)type size:(NSNumber*)size;
```
        parameters -
            roomName - should be alphanumeric characters without special characters.
            bitrate - video bandwidth for call in kbps. 512 represents 512kbps video bandwidth.
                      Note: total bandwidth = video bandwidth + audio bandwidth + control bandwidth
            frameRate - accepted values [@"low", @"high"]. low corresponds to 15 and high corresponds to 30.
            type - accepted values [@"peer", @"multiway", @"broadcast"].
                   @"peer" - peer to peer call.
                   @"multiway" - group calling up to 13 participants.
                   @"broadcast" - broadcast calling. Stream a single participant video to many people.
            size - Number of participants for conference room. Maximum value is 13 for multiway, peer calling.

5)
```objective-c
-(void) invite:(NSString *)roomName emailList:(NSString *)emailList;
```
        parameters -
            roomName - name of the room to join.   
            emailList - is a list of email separated by commas.
                        example: @"info@1click.io,developer@1click.io,support@1click.io"

6)
```objective-c
-(BOOL) muteAudio:(BOOL)enable;
```
        parameters -
            enable - if true mute microphone, false un-mute microphone.
        returns - True value is returned if operation succeeded, False on failure. 
7)
```objective-c
-(void) hideLocalVideo:(BOOL)enable;
```
        parameters -
            enable - if true hides local video, false show local video.

8)
```objective-c
-(void) switchCamera;
```
        Switch between front and back camera during video call.

9)
```objective-c
-(void) rotateVideo;
```
        On device rotation, rotates videoFrame to maintain world is always up during video call.